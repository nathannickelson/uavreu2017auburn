#define PI 3.14159265

#include "AvoidanceBearingAngle.h"

/*
2017 Nathan Nickelson
*/

double mag(glm::vec3 vektor);
double angleFromVectors(glm::vec3 vektor1, glm::vec3 vektor2);
double cam2BlobAngle(BlobInfo &blob);
double distanceFormula(glm::vec3 pos1, glm::vec3 pos2);

double const ADJX = 480.0;					// blob info X and Y are in screen coordinates
double const ADJY = 270.0;					// these numbers are half the screen dimensions
											// If the screen dimensions are changed from 960X540
											// these numbers will have to be changed


void AvoidanceBearingAngle::reactToBlob(BlobInfo info, Camera & camera, int index) {
		
	double facingCamToBlob, facingCamToWaypoint, blobToWaypoint;
	facingCamToBlob = cam2BlobAngle(info);								// retrieves the horizontal angle from center of blob screen
	double bearAngle = camera.getBearingAngle();						// retrieves the bearing angle
	
	Waypoint *target = camera.GetPath()->GetActiveWaypoint();			// Waypoint vector from the origin
	glm::vec3 vecToWayPoint = glm::vec3(
		target->GetPosition().x - camera.GetPosition().x,
		0.0f,
		target->GetPosition().z - camera.GetPosition().z);				// The vector from the camera's position to the Waypoint
	glm::vec3 direction = camera.GetCurrentDirectionFlat();				// The camera facing vector without in the x-z direction

	glm::vec3 position = camera.GetPosition();

	// angle between 2 vectors
	facingCamToWaypoint = angleFromVectors(vecToWayPoint, direction);
	blobToWaypoint = facingCamToWaypoint - facingCamToBlob;
	double cs = cos((bearAngle - facingCamToBlob) * PI / 180.0);
	double sn = sin((bearAngle - facingCamToBlob) * PI / 180.0);

	//******************************************************************
	
	//std::cout << "********************************" << std::endl;
	//std::cout << "magnitude direction - " << mag(direction) << std::endl;
	//std::cout << "direction.x = " << direction.x << " ------ direction.z = " << direction.z << std::endl;
	
	glm::vec3 camFaceToBlob = glm::vec3((direction.x * cs - direction.z * sn), (direction.y), (direction.x * sn + direction.z * cs));
	//camFaceToBlob =  glm::vec3(camFaceToBlob.x/ mag(camFaceToBlob), camFaceToBlob.y/ mag(camFaceToBlob), camFaceToBlob.z/ mag(camFaceToBlob));

	//std::cout << "camFaceToBlob = (" << camFaceToBlob.x << ", " << camFaceToBlob.y << ", " << camFaceToBlob.z << ")" << std::endl;
	glm::vec3 newWayPos = position;
	camera.setCamCycle(camera.getCamCycle() + 1);

	if (abs(blobToWaypoint) < 6.0 && abs(facingCamToWaypoint) < bearAngle && camera.getCamCycle()%7 == 0 && info.radius > 28) {
		//std::cout << "Setting new Waypoint" << std::endl;
		//std::cout << " origX = " << info.originalPositionX << " ======== currentX = " << info.currentPositionX << std::endl;
		if (info.originalPositionX > info.currentPositionX && bearAngle < 8.0) {
			//std::cout << "strafe = 1" << std::endl;
			camera.setStrafe(1);
		}else if (info.originalPositionX < info.currentPositionX && bearAngle < 8.0) {
			//std::cout << "strafe = ------1" << std::endl;
			camera.setStrafe(-1);
		}
		//std::cout << "camFace = " << camFaceToBlob.x << std::endl;
		if (abs(camFaceToBlob.x) > 0.03) {
			newWayPos = glm::vec3(position.x + (camera.getStrafe() * 300.0f * camFaceToBlob.x), position.y, (position.z + 300.0f*camFaceToBlob.z));
			if ((abs(angleFromVectors((newWayPos-camera.GetPosition()), camera.GetCurrentDirectionFlat())) < 110.0)
				&& (distanceFormula(camera.GetPosition(), camera.GetPath()->GetActiveWaypoint()->GetPosition())) > 40) {
				camera.GetPath()->SetAvoidanceWaypoint(new Waypoint(newWayPos));	
				bearAngle = min(bearAngle + 2.0, info.radius / 3.0);
				camera.setBearingAngle(bearAngle);
				camera.setCamStraight(false);
			}
		}
	}
	if (camera.getCamCycle() % 20 == 0 && bearAngle > 6.0) {
		camera.setBearingAngle(bearAngle - 0.10);
	}
	
	
	/*if ((camera.getCamStraight() == false) && camera.GetPath()->GetNextPathWaypoint()->Equals(camera.GetPath()->GetActiveWaypoint())
		||( abs(camera.GetPosition().x - camera.GetPath()->GetNextPathWaypoint()->GetPosition().x) > 700.0 
			&& (!camera.GetPath()->GetNextPathWaypoint()->Equals(camera.GetPath()->GetActiveWaypoint())))) {
		//stop turning and go straight
		camera.setCamStraight(true);
		//std::cout << "straight waypoint" << std::endl;
		glm::vec3 camD = camera.GetCurrentDirectionFlat();
		camD = glm::vec3(camD.x*500.0, camD.y*500.0, camD.z*500.0);
		glm::vec3 pos = camera.GetPosition();
		glm::vec3 goStraight = camD + pos;
		camera.GetPath()->SetAvoidanceWaypoint(new Waypoint(goStraight));
	}*/
	
	
	info.oldBlobAngle = facingCamToBlob;
	info.originalPositionX = info.currentPositionX;
	
	
	//std::cout << "blob #" << index << " size = " << info.currentSize << std::endl
		//<< "Position is (" << x << ", " << y << ")" << std::endl
		//<< "Angle of Object from forward vector = " << facingCamToBlob << std::endl
		//<< "Flag position = (" << target->GetPosition().x << ", " << target->GetPosition().y << ", " << target->GetPosition().z << ")"
		//<< "Waypoint angle is: " << facingCamToWaypoint << std::endl
		//<< "blob to Waypoint = " << blobToWaypoint << std::endl
		//<< std::endl;

}


//Vector Magnitude Function
double mag(glm::vec3 vektor) {
	double magnitude = sqrt((vektor.x*vektor.x) + (vektor.y*vektor.y) + (vektor.z*vektor.z));
	return magnitude;
}

//Angles from 2 vectors Function
double angleFromVectors(glm::vec3 vektor1, glm::vec3 vektor2) {
	double angle = acos(glm::dot(vektor1, vektor2) / (mag(vektor1)* mag(vektor2)))*(180 / PI);
	return angle;
}

//Calculates the horizontal screen angle of a blob from center
double cam2BlobAngle(BlobInfo &blob) {
	double x = blob.currentPositionX - ADJX;	// conversion from screen coordinates to cartesian
	double y = blob.currentPositionY - ADJY;	// (0,0) being the center of the screen

	double blobAngle = (asin(x / 1090.00))*(180 / PI);// 1090 is used as the number that most closely matches a 25 degree
												// deviation from center when the blob is at the edge of the screen
												// if a different FOV other than 50 is used, this number will 
												// likely have to change
	return blobAngle;
}

double distanceFormula(glm::vec3 pos1, glm::vec3 pos2) {
	double distance = sqrt(pow((pos1.x - pos2.x), 2) + pow((pos1.y - pos2.y), 2) + pow((pos1.z - pos2.z), 2));
	return distance;
}
