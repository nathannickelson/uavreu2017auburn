#pragma once

#include "AvoidanceAlgorithm.h"
#include "../AircraftTable.h"
#include "../Utility.h"

// Bearing angle avoidance Algorithm 2017

class AvoidanceBearingAngle : AvoidanceAlgorithm {
public:
	void reactToBlob(BlobInfo info, Camera & camera, int index);
};
