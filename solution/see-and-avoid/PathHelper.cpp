#include "PathHelper.h"

PathHelper::PathHelper()
{
	this->scale = 1.0;
}
PathHelper::PathHelper(GLfloat scale)
{
	this->scale = scale;
}

PathHelper::~PathHelper() {};

Path * PathHelper::GetLinearPath() {
	vector<Waypoint *> waypoints;
	// Path 1 - Straight line with circly turnabouts for our camera
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, -500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(400.0f, 0.0f, -900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(300.0f, 0.0f, -1400.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, -500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, 500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-400.0f, 0.0f, 900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-300.0f, 0.0f, 1400.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, 1000.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(0.0f, 0.0f, 500.0f)));
	return new Path(waypoints, 40.0f);
}


Path * PathHelper::GetCircularPath() {
	vector<Waypoint *> waypoints;
	int numPts = 15;
	float angleStep = 360.0 / numPts;
	float radius = -1500.0;
	for (int i = 0; i < numPts; i++) {
		float angle = (angleStep * i)*3.14159 / 180;
		float x = cos(angle)*radius;
		float z = sin(angle)*radius;
		waypoints.push_back(new Waypoint(this->scale*glm::vec3(x, 0.0f, z)));
	}
	return new Path(waypoints, 40.0f);
}


Path * PathHelper::GetFigureEightPath() {
	vector<Waypoint *> waypoints;
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	return new Path(waypoints, 40.0f);
}

Path* PathHelper::GetOnePath() {
	vector<Waypoint *> waypoints;
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -2500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 2500.0f)));
	return new Path(waypoints, 40.0f);
}

//path through airport space
Path * PathHelper::GetAirportPath() {
	vector<Waypoint *> waypoints;
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, -1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(1900.0f, 0.0f, 1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, -1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, 1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(1900.0f, 0.0f, -1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, 1900.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, 0.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(1900.0f, 0.0f, 0.0f)));
	waypoints.push_back(new Waypoint(this->scale*glm::vec3(-1900.0f, 0.0f, 0.0f)));
	return new Path(waypoints, 40.0f);
}

Path * PathHelper::GetBackandForth() {
	vector<Waypoint *> waypoints;
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -2000.0f)));
	return new Path(waypoints, 40.0f);
}


//**************************************** SELECTS WAY POINTFLIGHT CHOICE ********************************************
//************************* this function is split into two functions becuase the selection screen is put into the
//************************* the background if the choice is made later, this stores the choice and the second function
//************************* setPath() then sets the waypoints after the windows have been setup **********************

void PathHelper::choosePath(int &choice) {
	std::cout << std::endl << std::endl
		<< "[Monday version] What waypoint flight path would you like to take: " << std::endl << std::endl
		<< "[1] Diagonal Path" << std::endl
		<< "[2] Circular Path" << std::endl
		<< "[3] Figure Eight Path" << std::endl
		<< "[4] Back and forth between two points" << std::endl
		<< "[5] Square Path" << std::endl;
	do {
		std::cin >> choice;
	} while ((choice < 1 || choice > 5) || (choice != int(choice)));
}


// ***************************************** SETS WAYPOINT FLIGHT CHOICE ********************************************
void PathHelper::setPath(int choice, Camera &cam){
	switch (choice) {
	case 1:
		cam.SetPath(GetLinearPath());
		break;
	case 2:
		cam.SetPath(GetCircularPath());
		break;
	case 3:
		cam.SetPath(GetFigureEightPath());
		break;
	case 4:
		cam.SetPath(GetOnePath());
		break;
	case 5:
		cam.SetPath(GetAirportPath());
		break;
	case 6:
		cam.SetPath(GetBackandForth());
		break;
	}
}

