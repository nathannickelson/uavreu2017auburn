#include "Aircraft.h"

Aircraft::Aircraft(glm::vec3 startingPos, Path & path, GLchar * modelFilePath, AircraftScale acScale) : model(modelFilePath)
{
	//std::cout << "making aircraft (aircraft.cpp line 5)" << std::endl;

	this->position = startingPos;
	this->pitch = 0.0f;
	this->roll = 0.0f;
	this->yaw = 0.0f;
	Model aircraftModel(modelFilePath);
	//std::cout << "making aircraft (aircraft.cpp line 11)" << std::endl;
	this->model = aircraftModel;
	wpComplete = 0;

	double scale;
	if (acScale == AircraftScale::big) {
		scale = AircraftTable::getBoeing747().wingspan / AircraftTable::GetPlaneModelWingspan();
		this->collisionRadius = AircraftTable::getBoeing737().wingspan;
	}
	else if (acScale == AircraftScale::med) {
		scale = AircraftTable::getBoeing737().wingspan / AircraftTable::GetPlaneModelWingspan();
		this->collisionRadius = AircraftTable::getMidsizeJet().wingspan;
	}
	else {
		scale = AircraftTable::getMidsizeJet().wingspan / AircraftTable::GetFighterModelWingspan();
		this->collisionRadius = AircraftTable::getCessna172().wingspan;
	}
	this->scale = scale;

	this->hasCollided = false;

	this->path = path;

	this->autonomousMode = true;
	//std::cout << "made it through aircraft.cpp line33" << std::endl;

}

Aircraft::Aircraft(glm::vec3 position, GLchar* filepath, AircraftScale acScale) : model(filepath)
{
	this->position = position;
	this->pitch = 0.0f;
	this->roll = 0.0f;
	this->yaw = 0.0f;
	Model aircraftModel(filepath);
	this->model = aircraftModel;
	wpComplete = 0;

	double scale;
	if (acScale == AircraftScale::big) {
		scale = 1;//AircraftTable::getBoeing747().wingspan / AircraftTable::GetPlaneModelWingspan();
		this->collisionRadius = 225; //AircraftTable::getBoeing747().wingspan;
	}
	else if (acScale == AircraftScale::med) {
		scale = 1;//AircraftTable::getMidsizeJet().wingspan / AircraftTable::GetFighterModelWingspan();
		this->collisionRadius = 75; // AircraftTable::getMidsizeJet().wingspan;
	}
	else {
		scale = 1;// AircraftTable::getCessna172().wingspan / AircraftTable::GetCessnaModelWingspan();
		this->collisionRadius = 50; // AircraftTable::getCessna172().wingspan;
	}
	this->scale = scale;

	this->hasCollided = false;

	this->autonomousMode = false;
}

void Aircraft::SetOrientation(GLfloat pitch, GLfloat yaw, GLfloat roll)
{
	this->pitch = pitch;
	this->yaw = yaw;
	this->roll = roll;
}

void Aircraft::SetPosition(glm::vec3 position)
{
	this->position = position;
}

void Aircraft::SetSpeed(GLfloat newSpeed)
{
	this->speed = newSpeed;
}

glm::vec3 Aircraft::GetCurrentDirection() {
	// determine aircraft direction from pitch, roll, and velocity and update position
	glm::vec3 direction;
	direction.x = cos(glm::radians(this->pitch)) * cos(glm::radians(this->yaw + 90));
	direction.y = sin(glm::radians(this->pitch));
	direction.z = cos(glm::radians(this->pitch)) * sin(glm::radians(this->yaw + 90));
	return glm::normalize(direction);
}

bool Aircraft::IsAutonomous()
{
	return this->autonomousMode;
}

Path * Aircraft::GetPath()
{
	return &this->path;
}

void Aircraft::scales(AircraftScale &scale1, AircraftScale &scale2) {
	int size = rand() % 3;
	if (size == 0) {
		scale1 = AircraftScale::big;
		scale2 = AircraftScale::med;
	}
	else if (size == 1) {
		scale1 = AircraftScale::med;
		scale2 = AircraftScale::small;
	}
	else {
		scale1 = AircraftScale::small;
		scale2 = AircraftScale::big;
	}
}

Aircraft::~Aircraft()
{
}
