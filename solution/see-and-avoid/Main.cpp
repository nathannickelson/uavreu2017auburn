#pragma once

/*
Modifications Done by Nathan Nickelson and Huy Duong
Auburn REU on Smart UAVs 2017
Research Coordinator: Dr. Richard Chapman
REU Director: Dr. Saad Biaz


Original Created by:
Project created by Andy Morgan and Zach Jones 
Auburn REU on SMART UAVs 2016
Research Coordinator: Dr. Richard Chapman
REU Director: Dr. Saad Biaz
*/

#define _ITERATOR_DEBUG_LEVEL 0
#define PI 3.14159265
#define DEBUG false

#include <thread>
#include <mutex>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>

#define GLEW_STATIC
#include <glew.h>
#include <glfw3.h>

#include <iostream>
#include <cstdio>
#include <vector>
#include <sstream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "KeyboardHandler.h"
#include "Skybox.h"
#include "Shader.h"
#include "Camera.h"
#include "Aircraft.h"
#include "PlaneDrawer.h"
#include "PathHelper.h"
#include "PlaneGenerator.h"
#include "PrintToFile.h"
#include "Algorithms\DoNothingAvoidance.h"
#include "Algorithms\AtcAvoidance.h"
#include "Algorithms\AvoidanceDistanceAgnostic.h"
#include "Algorithms\AvoidanceWithDistance.h"
#include "Algorithms\AvoidanceAgnosticElevation.h"
#include "Algorithms\AvoidanceBearingAngle.h"
#include "VisionProcessor.h"
#include "ContourVisionProcessor.h"

//TODO: remove

#include "Cube.h"
#include "CubeDrawer.h"
#include "Utility.h"

using namespace std;

/**************************** forward declarations go here **************************************************/
//Determines if planes are randomly generated
bool RANDOM = false;

// functions to run in threads
int renderScene();
int processScene();
mutex semaphore;
cv::Mat img; // this will contain the current view rendered by openGL.  Must be locked before any R/W operations
bool renderingStopped = false;

// callback functions
static void error_callback(int error, const char* description);
void drawAirplane(Mat & drawer, Camera camera, Aircraft * myplane, Scalar color, bool isCameraObject); 
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void on_trackbar(int, void*);

//variables for top-down plane tracker
vector< Mat> planePaths;
vector<Mat> PlanePathMatrices;
int planeSelection = 0;
int widthOfAirspace = 6000;
int oldWPC, newWPC, choice, choice2, choice3;				//old and new waypoint counter, menu choice
GLuint width, height;

int collisionCount = 0;

vector< Aircraft*> myplanes; // planes to render
Camera camera; // camera object

//************************** intializing the avoidance algorithms to use later ******************************/
DoNothingAvoidance dna = DoNothingAvoidance();
AvoidanceWithDistance awd = AvoidanceWithDistance();
AvoidanceDistanceAgnostic ada = AvoidanceDistanceAgnostic();
AvoidanceAgnosticElevation aae = AvoidanceAgnosticElevation();
AtcAvoidance atc = AtcAvoidance(); 
AvoidanceBearingAngle abba = AvoidanceBearingAngle();

/***************************** End forward declarations ********************************************************/

int main() {
	srand(time(NULL));
	PrintToFile::clearFile();
	PrintToFile::clearDebugFile();	
	//clear the folder holding collision photos - this will have to change on *nix systems
	system("del /Q \"collisions\"");

	thread renderThread(renderScene);

	thread processThread(processScene);

	if (renderThread.joinable()) {
		renderThread.join();
	}
	if (processThread.joinable()) {
		processThread.join();
	}

	glfwTerminate();
	return 0;
}

//################################################################################################################
//#########             THREAD ONE -- RENDER SCENE       #########################################################
//################################################################################################################

int renderScene() {
	//set error CB function
	glfwSetErrorCallback(error_callback);

	//try to intialize GLFW
	if (glfwInit() != GL_TRUE) {
		exit(EXIT_FAILURE);
	}
	//GLFW initialized now.  Set window hints for OpenGL v3.3 and make window fixed size
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // this will cause openGL to raise an error when we try to call legacy library functions
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // no resizing for you, Mr. End-User

	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	width = mode->width;
	height = mode->height;

	//Time to create a window with GLFW
	GLFWwindow* window = glfwCreateWindow(width / 2, height / 2, "See and Avoid Sim", nullptr, nullptr);
	
	if (window == nullptr) //ensure the window was initialized
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	//hide the window for setup
	glfwHideWindow(window);
	glfwMakeContextCurrent(window);

	//Initialize GLEW before calling any openGL functions
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	//setup the key callback
	glfwSetKeyCallback(window, key_callback);

	//set the clear color
	glClearColor(0.8f, 0.9f, 1.0f, 1.0f); // sky blue-ish;  We should never see this if the skybox is working

	//tell openGL to use depth testing
	glEnable(GL_DEPTH_TEST);

	Shader skyboxShader(".\\Shaders\\Skybox\\skybox.vs", ".\\Shaders\\Skybox\\skybox.fs");
	Skybox * skybox = new Skybox(skyboxShader);

	// PathHelper for preloaded paths
	PathHelper * pathHelper = new PathHelper(widthOfAirspace / 4000.0f);

	// create a plane that follows a path
	Shader planeShader(".\\Shaders\\Aircraft\\aircraft.vs", ".\\Shaders\\Aircraft\\aircraft.fs");
	
	//Create Planes Before Drawing any new windows
	//PlaneGenerator planeGenerator(RANDOM, widthOfAirspace);

	
	//**************************************************************************************************************
	//********************************************   MENU OPTIONS   ************************************************
	//**************************************************************************************************************

	int choice = choice2 = choice3 = -1;
	AircraftScale scale1, scale2;
	Aircraft::scales(scale1, scale2);

	PlaneGenerator::chooseSim(widthOfAirspace, scale1, scale2, choice);		//Call function to choose simulation Menu
	Camera::setSimChoice(choice);
	
	std::cout << std::endl << std::endl					//************** Avoidance Choices Menu***********************
		<< "[Monday version] What Avoidance algorithm would you like to use: " << std::endl << std::endl
		<< "[1] Do nothing - crash into everything" << std::endl
		<< "[2] Estimated distance" << std::endl
		<< "[3] Distance agnostic" << std::endl
		<< "[4] Distance agnostic with elevation" << std::endl
		<< "[5] atc  ... ?" << std::endl
		<< "[6] Constant Bearing Angle (2017)" << std::endl;
	
	do {
		std::cin >> choice2;
	} while ((choice2 < 1 || choice2 > 6) || (choice2 != int(choice2)));
			
	myplanes = PlaneGenerator::getPlanes();	
	if (choice != 9 && choice != 10 && choice != 11 && choice != 7) {
		pathHelper->choosePath(choice3);				//Choose a path for your camera to follow;
	}
	if (choice == 9 || choice == 10 || choice == 11 || choice == 7) {
		choice3 = 6;									//Automatically chooses a camera path
	}

	// we have to create openCV windows in this thread!
	
	//***************************** BLOB DETECTION WINDOW - USED TO ASSESS THREATS ********************************
	namedWindow("Blob Detection", CV_WINDOW_NORMAL);
	cv::resizeWindow("Blob Detection", width/2, height/2);
	cv::moveWindow("Blob Detection", width/2, 0);

	
	//***************************** WINDOW SETUP FOR OVERHEAD VIEW OF ALL AIRCRAFT *******************************
	//**************************************** AND THEIR PATHWAYS ************************************************

	PlanePathMatrices = PlaneGenerator::getPlanePaths();
	namedWindow("Plane Paths", CV_WINDOW_AUTOSIZE);
	cv::moveWindow("Plane Paths", width/2, height/2);
	createTrackbar("Plane Select: ", "Plane Paths", &planeSelection, PlaneGenerator::getPlanePaths().size() - 1, on_trackbar);	

	Texture defaultPlaneTexture(".\\asset\\container.jpg");
	PlaneDrawer * planeDrawer = new PlaneDrawer(defaultPlaneTexture, planeShader);

	// create camera and path for camera (our plane)
	float scale = 1; //widthOfAirspace / 4000.0; // 4000 was default width of airspace
	
	
	if (choice != 7) {
		camera = Camera(width, height, glm::vec3(0.0f, 0.0f, 2000.0f));	
		camera.ActivateAutonomousMode();										
	}
	if (choice == 7) {
		camera = Camera(width, height, glm::vec3(00.0f, 120.0f, 200.0f));		// choice 7 is display airplanes mode
		KeyboardHandler::fixedWing = !KeyboardHandler::fixedWing;				// default is zero speed and auto-pilot off
		camera.setSpeed(0.0f);
	}
	if (choice == 9 || choice == 10 || choice == 11) {
		camera.changeIPosition(glm::vec3(0.0f, 0.0f, 2000.0f));
		camera.Reset();
		if (choice == 10) {
			camera.changeMaxSpeed(100.0);
			camera.setSpeed(100.0);
		}
	}
	camera.setSimChoice(choice);				//Set simulation choice
	
	pathHelper->setPath(choice3, camera);		//Set pathway for you

	//Show the Window again once we are ready
	glfwShowWindow(window);
	
	//************************************************************************************************
	//************** LOOPING THE THE VIDEO UNTIL 'ESCAPE' KEY IS PRESSED******************************
	//************** make sure all preliinary setup is in place before this step**********************
	//************** mutex freezes a frame to send over to the openCV process thread *****************
	//************************************************************************************************

	while (!glfwWindowShouldClose(window))
	{
		glm::vec3 position = camera.GetPosition();
		if (camera.getCamCycle() % 2500 == 0 && choice2!=2) {
			std::cout << "cam position = " << position.x << "-" << position.y << "-" << position.z << std::endl;
		}
		// setting reset conditions in case of craft wandering far away or goes NAN
		if (abs(position.x) > 10000 || abs(position.y) > 10000 || abs(position.z) > 10000 || isnan(position.x)) {
			std::cout << "camera went NaN" << std::endl;
			camera.changeIPosition(glm::vec3(0.0f, 0.0f, 2200.0f));
			glm::vec3 newPos = glm::vec3(0.0f, 0.0f, 2000.0f);
			camera.GetPath()->SetAvoidanceWaypoint(new Waypoint(newPos));
			camera.Reset();
		}
		if (KeyboardHandler::keys[GLFW_KEY_0]) {							//************** GENERATE A RANDOM PLANE
			if (choice != 9) {
				PlaneGenerator::generateRandomPlanes(widthOfAirspace, 1);
			}
			if (choice == 9) {
				PlaneGenerator::circleGauntlet(widthOfAirspace, 1);
			}
			camera.setAddPlane(true);
		}
		if (KeyboardHandler::keys[GLFW_KEY_COMMA]) {						//*** ENABLE/DISABLE AUTONOMOUS MODE 
			camera.DeactivateAutonomousMode();
		}
		if (KeyboardHandler::keys[GLFW_KEY_PERIOD]) {
			camera.ActivateAutonomousMode();
		}

		oldWPC = camera.getWaypointsCompleted();
		// poll for events
		glfwPollEvents();
		// clear screen and render clear color (this should be covered by skybox)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen - will be filled with clear color set above
		// the current time in seconds
		GLfloat timeValue = (GLfloat)glfwGetTime();

		camera.DoMovement(timeValue);

		glm::mat4 projection, view;

		// draw other (non-skybox) objects below
		view = camera.GetCameraViewMatrix();
		projection = camera.GetProjectionMatrix();

		//draw skybox
		skybox->Draw(view, projection);

		//draw aircraft
		planeDrawer->Draw(camera, camera.GetPosition(), timeValue, myplanes);

		if (choice == 10 && camera.getAddPlane() == true) {						// adds a plane obstacle when set to TRUE
			PlaneGenerator::slowRoll(6000, 1);
		}
		if (choice == 11 && camera.getAddPlane() == true) {
			PlaneGenerator::circleGauntlet(6000, 1);
		}

		//OpenCV stuf
		//create an empty matrix to hold frame data
		semaphore.lock();
		img = cv::Mat(height / 2, width / 2, CV_8UC3);
		//use fast 4-byte alignment if possible
		glPixelStorei(GL_PACK_ALIGNMENT, (img.step & 3) ? 1 : 4);
		//set length of one complete row in destination data (doesn't need to equal img.cols)
		glPixelStorei(GL_PACK_ROW_LENGTH, img.step / img.elemSize());
		glReadPixels(0, 0, img.cols, img.rows, GL_BGR, GL_UNSIGNED_BYTE, img.data);
		cv::flip(img, img, 0);
		resize(img, img, Size(960, 540));

		//check if planedrawer has recently detected a collision and export the current openGL view to a file
		if (planeDrawer->recentCollisionTime != nullptr) {
			collisionCount++;
			std::cout << "==================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Collision #" << collisionCount << " reported to file." << std::endl;

			stringstream filename; tm* ltm = planeDrawer->recentCollisionTime;
			filename << "collisions/" << ltm->tm_hour << "_" << ltm->tm_min << "_" << ltm->tm_sec + 1 << ".jpg";
			imwrite(filename.str(), img);
			planeDrawer->recentCollisionTime = nullptr;
		}
		semaphore.unlock();
		//std::cout << "plane path matrices (main.cpp line 317)" << PlanePathMatrices.size() << std::endl;
		on_trackbar(1, 0);
		
		// swap buffers to display what we just rendered on the back buffer
		glfwSwapBuffers(window);
	}
	
	glfwTerminate();
	renderingStopped = true;
	return 0;
}


//***********************************************************************************************************
//********************************** PROCESSING THREAD FOR 'SEE'ING OBSTACLES********************************
//********************************** uses threat assessment based on value of 'choice2' *********************
//********************************** also implements the 'avoidance' of threats******************************
//***********************************************************************************************************
int processScene() {
	
	Mat frame;
	ContourVisionProcessor processor = ContourVisionProcessor();
	double focalLengthSum = 0;
	int focalLengthMeasurements = 0;
	while (!renderingStopped) {
		semaphore.lock();
		frame = img;
		semaphore.unlock();
		vector<BlobInfo> blobs = processor.ProcessScene(frame);
		for (unsigned int i = 0; i < blobs.size(); i++) {
			switch (choice2) {
			case 1: 
				dna.reactToBlob(blobs[i], camera, i);
				break;
			case 2:
				awd.reactToBlob(blobs[i], camera, i);
				break;
			case 3:
				ada.reactToBlob(blobs[i], camera, i);
				break;
			case 4:
				aae.reactToBlob(blobs[i], camera, i);
				break;
			case 5:
				atc.reactToBlob(blobs[i], camera, i);
				break;
			case 6:
				abba.reactToBlob(blobs[i], camera, i);
				break;
			}	
		}		
	}	
	return 0;
}



//************************************ TRACKBAR SETUP FOR PLANES AND OWN AIRCRAFT **********************************************

void on_trackbar(int , void*) {
	//****************** First section here redraws the map when another random plane is added ***********************
	//****************** to simulation 5: random planes. (may adjust later to accomodate other sims) ****************
	
	//##############################################################################################################
	// This section allows the overhead map to be updated when a new plane is added. Various simulation options could
	// add a plane at specified intervals (like option 9) or a plane could be randomly added when the '0' key is
	// pressed. Otherwaise the toolbar will not register new planes being added.
	if (camera.getSimChoice() == 5 || camera.getSimChoice() == 9) {
		newWPC = camera.getWaypointsCompleted();
	}else {
		newWPC = oldWPC;
	}
	if ((newWPC > oldWPC) || (camera.getAddPlane() == true)) {
		camera.setAddPlane(false);
		destroyWindow("Plane Paths");
		//system("pause");
		myplanes = PlaneGenerator::getPlanes();
		PlanePathMatrices = PlaneGenerator::getPlanePaths();
		std::cout << "PlanePathMatrices = " << PlanePathMatrices.size() << std::endl;
		namedWindow("Plane Paths", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("Plane Paths", width / 2.0, height / 2.0);
		createTrackbar("Plane Select: ", "Plane Paths", &planeSelection, PlaneGenerator::getPlanePaths().size() - 1, on_trackbar);
	}
	//end of map remake when plane is added section
	//###############################################################################################################################

	Mat local = PlanePathMatrices.at(planeSelection).clone();
	if (planeSelection == myplanes.size()) { //this is for the master matrix where we place all of our planes on a single matrix
		for (unsigned int i = 0; i < myplanes.size(); i++) {
			stringstream value; value << i;
			putText(local, value.str(), Point((myplanes.at(i)->position.x + widthOfAirspace / 2) / (widthOfAirspace / 500)-3, (myplanes.at(i)->position.z + widthOfAirspace / 2) / (widthOfAirspace / 500)+5), FONT_HERSHEY_SIMPLEX,0.7, Scalar(0, 0, 0),2,8,false);
		}
	}
	else { //this works for our single airplane matrix
		stringstream value; value << planeSelection;
		drawAirplane(local, camera, myplanes.at(planeSelection), Scalar(255, 0, 0), false);  //We will now place a plane on our path
		line(local, Point((myplanes.at(planeSelection)->position.x + widthOfAirspace / 2) / (widthOfAirspace / 500), (myplanes.at(planeSelection)->position.z + widthOfAirspace / 2) / (widthOfAirspace / 500)), Point((myplanes.at(planeSelection)->GetPath()->GetActiveWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500), (myplanes.at(planeSelection)->GetPath()->GetActiveWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500)),Scalar(255,0,0),1, CV_AA);
	}
	// we will now draw ourselves on the matrix, and our associated waypoints
	drawAirplane(local, camera, nullptr, Scalar(0, 0, 0), true);
	if (camera.GetPath() != nullptr && camera.GetPath()->GetActiveWaypoint() != nullptr) {
		//This will draw our waypoint
		rectangle(local, Point((camera.GetPath()->GetNextPathWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500), (camera.GetPath()->GetNextPathWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500) - 15), Point((camera.GetPath()->GetNextPathWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500) + 20, (camera.GetPath()->GetNextPathWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500) - 25), Scalar(180, 105, 255), -1);
		line(local, Point((camera.GetPath()->GetNextPathWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500), (camera.GetPath()->GetNextPathWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500)), Point((camera.GetPath()->GetNextPathWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500), (camera.GetPath()->GetNextPathWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500)-25), Scalar(0, 0, 0), 3);
		//This will draw our temporary waypoint if we have one
		if(camera.GetPath()->GetActiveWaypoint()->GetPosition() != camera.GetPath()->GetNextPathWaypoint()->GetPosition())
			circle(local, Point((camera.GetPath()->GetActiveWaypoint()->GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500), (camera.GetPath()->GetActiveWaypoint()->GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500)), 7, Scalar(180, 105, 255), -1);
	
	}	
	imshow("Plane Paths", local);

}


static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

//responds to keyboard events
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	KeyboardHandler::handle_key_press(window, key, scancode, action, mods);
	//handle escape key
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

void drawAirplane(Mat & drawer, Camera camera,Aircraft* myplane, Scalar color, bool isCameraObject) {

	if (isCameraObject) {
		float cameraX = (camera.GetPosition().x + widthOfAirspace / 2) / (widthOfAirspace / 500);
		float cameraZ = (camera.GetPosition().z + widthOfAirspace / 2) / (widthOfAirspace / 500);
		glm::vec3 upVec = glm::vec3(0.0f, 1.0f, 0.0f);
		glm::vec3 orthogonalVec = glm::cross(upVec, camera.GetCurrentDirection());
		line(drawer, Point(cameraX + 10 * camera.GetCurrentDirection().x, cameraZ + 10 * camera.GetCurrentDirection().z), Point(cameraX - 18 * camera.GetCurrentDirection().x, cameraZ - 18 * camera.GetCurrentDirection().z), color, 6);
		line(drawer, Point(cameraX + 12 * orthogonalVec.x, cameraZ + 12 * orthogonalVec.z), Point(cameraX - 12 * orthogonalVec.x, cameraZ - 12 * orthogonalVec.z), color, 8);
		line(drawer, Point(cameraX - 18 * camera.GetCurrentDirection().x + 6 * orthogonalVec.x, cameraZ - 18 * camera.GetCurrentDirection().z + 6 * orthogonalVec.z), Point(cameraX - 18 * camera.GetCurrentDirection().x - 6 * orthogonalVec.x, cameraZ - 18 * camera.GetCurrentDirection().z - 6 * orthogonalVec.z), color, 4);
	}
	else {
		float airplaneX = (myplane->position.x + widthOfAirspace / 2) / (widthOfAirspace / 500);
		float airplaneZ = (myplane->position.z + widthOfAirspace / 2) / (widthOfAirspace / 500);
		glm::vec3 upVec = glm::vec3(0.0f, 1.0f, 0.0f);
		glm::vec3 orthogonalVec = glm::cross(upVec, myplane->GetCurrentDirection());
		line(drawer, Point(airplaneX + 10 * myplane->GetCurrentDirection().x, airplaneZ + 10 * myplane->GetCurrentDirection().z), Point(airplaneX - 18 * myplane->GetCurrentDirection().x, airplaneZ - 18 * myplane->GetCurrentDirection().z), color, 6);
		line(drawer, Point(airplaneX + 12 * orthogonalVec.x, airplaneZ + 12 * orthogonalVec.z), Point(airplaneX - 12 * orthogonalVec.x, airplaneZ - 12 * orthogonalVec.z), color, 8);
		line(drawer, Point(airplaneX - 18 * myplane->GetCurrentDirection().x + 6 * orthogonalVec.x, airplaneZ - 18 * myplane->GetCurrentDirection().z + 6 * orthogonalVec.z), Point(airplaneX - 18 * myplane->GetCurrentDirection().x - 6 * orthogonalVec.x, airplaneZ - 18 * myplane->GetCurrentDirection().z - 6 * orthogonalVec.z), color, 4);
	}
}

//Old code. Moved for now, may be implemented later*******************************************************//


//TODO: remove
//cubeDrawer->Draw(view, projection, timeValue, cubesToDraw);

//camera.GetPath()->SetAvoidanceWaypoint(new Waypoint(glm::vec3(-100.0f, 0.0f, -1100.0f)));

//TODO: remove
/*Cube distCube1 = Cube(glm::vec3(200.0f, -15.0f, 20.0f));
Cube distCube2 = Cube(glm::vec3(-200.0f, -15.0f, 20.0f));
vector<Cube> cubesToDraw = vector<Cube>();
cubesToDraw.push_back(distCube1);
cubesToDraw.push_back(distCube2);

Shader cubeShader = Shader(".\\Shaders\\Cube\\cube.vs", ".\\Shaders\\Cube\\Cube.fs");
CubeDrawer * cubeDrawer = new CubeDrawer(defaultPlaneTexture, defaultPlaneTexture, cubeShader);
*/