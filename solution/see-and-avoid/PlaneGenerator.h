
/*
Class is used for building planes to put in the virutal world. We can either build planes with a given set of waypoints,  or build a set number of
planes randomly in the environment. Each has a particular research point to focus on.
*/


#pragma once
#include <vector>
#include "Aircraft.h"
#include "AircraftTable.h"

using namespace std;
#include <opencv2/highgui/highgui.hpp>
#include <opencv2\opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdlib.h>     

using namespace cv;
static int PARTS = 4;							//USED TO DIVIDE CIRCULAR WAYPOINT PATHS INTO PARTS

class PlaneGenerator
{
public:
	//PlaneGenerator(bool random, int widthOfAirpace);
	static vector< Aircraft*> getPlanes();
	static vector< Mat> getPlanePaths();
	
	//Menu for Simulation choice
	static void chooseSim(int widthOfAirspace, AircraftScale scale1, AircraftScale scale2, int& choice);

	//2017 edition testing (OPTIONS 7,8,9,10,11)
	static void displayPlanes(int width);
	static void projectRunway(int width, int numStart);
	static void circleGauntlet(int width, int numStart);
	static void slowRoll(int width, int numStart);
	
	// circle division variable (PARTS)
	static int getParts();

	//(OPTIONS 1,2,3)
	static void generate1Plane(int width, int angle);
	static void generate2Planes(int width, AircraftScale scale1, AircraftScale scale2, int anglePlane1, int anglePlane2);
	static void generate3Planes(int width, AircraftScale scale1, AircraftScale scale2, AircraftScale scale3, int anglePlane1, int anglePlane2, int anglePlane3);

	//(OPTIONS 3,4,5,6)
	static void generateAirspacePlanes(int widthOfAirspace);
	static void generateRandomPlanes(int widthOfAirspace, int numPlanes);
	static void generateApproachingPlanes(int widthOfAirspace);
	static void generateAirportPlanes(int width);

private:
	//static vector< Aircraft*> myPlanes;
	//static vector< Mat> planePaths;
	//static vector<Waypoint *> waypoints;
	//static vector<Point> points;
	//static vector<vector<Point>> planePoints;
	//static int widthOfAirspace;
	static void MyLine(Mat img, Point start, Point end, int red, int green, int blue, int widthOfAirspace);
	static void DrawPathsOnMatrix(int widthOfAirspace);

};

