//Utility class for creating paths
#pragma once

#include "Path.h"
#include "Camera.h"

class PathHelper
{
public:
	PathHelper();
	PathHelper(GLfloat scale);
	~PathHelper();

	Path* GetLinearPath();
	Path* GetCircularPath();
	Path* GetFigureEightPath();
	Path* GetOnePath();
	Path* GetAirportPath();
	Path* GetBackandForth();

	void choosePath(int &choice);
	void setPath(int choice, Camera &cam);
	

private:
	GLfloat scale;
	
};

