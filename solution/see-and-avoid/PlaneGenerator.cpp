#define PI 3.14159265

#include "PlaneGenerator.h"
#include "Camera.h"
#include <vector>
#include <sstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>

using namespace std;

static vector< Aircraft*> myPlanes;
static vector< Mat> planePaths;
static vector<Waypoint *> waypoints;
static vector<Point> points;
static vector<vector<Point>> planePoints;

/////////////////////////////////////////////////////////////////////////////////
//// function for approaching planes has not been included in the choices ///////
////////////////////////////////////////////////////////////////////////////////

/*Generate Planes for later use*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 1 - GENERATE ONE PLANE //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generate1Plane(int width, int angle) {
	int widthOfAirspace = width;

	waypoints.push_back(new Waypoint(glm::vec3(-5 * 1000 * sin(angle *PI / 180), 0.0f, -1000 + 5 * 1000 * cos(angle *PI / 180))));
	Path planePath = Path(waypoints, 20.0f);
	Aircraft * plane;
	int planeType = rand() % 3;
	if (planeType == 0)
		plane = new Aircraft(glm::vec3(1000 * sin(angle *PI / 180), 0.0f, -1000 - 1000 * cos(angle *PI / 180)), planePath, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if (planeType == 1)
		plane = new Aircraft(glm::vec3(1000 * sin(angle *PI / 180), 0.0f, -1000 - 1000 * cos(angle *PI / 180)), planePath, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::med);
	else
		plane = new Aircraft(glm::vec3(1000 * sin(angle *PI / 180), 0.0f, -1000 - 1000 * cos(angle *PI / 180)), planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::med);
	plane->SetSpeed(50.0f);
	plane->SetOrientation(0.0f, (GLfloat)angle, 0.0f);
	myPlanes.push_back(plane);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	DrawPathsOnMatrix(widthOfAirspace);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 2 - GENERATE TWO PLANE //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generate2Planes(int width, AircraftScale scale1, AircraftScale scale2, int anglePlane1, int anglePlane2) {
	int widthOfAirspace = width;

	waypoints.push_back(new Waypoint(glm::vec3(-5 * 1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 +5* 1000 * cos(anglePlane1 *PI / 180))));
	Path planePath = Path(waypoints, 20.0f);
	Aircraft * plane;
	if (scale1 == AircraftScale::big)
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if ((scale1 == AircraftScale::med))
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	else
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::med);
	plane->SetSpeed(50.0f);
	plane->SetOrientation(0.0f, (GLfloat)anglePlane1, 0.0f);
	myPlanes.push_back(plane);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(-5*1000 * sin(anglePlane2 *PI / 180), 0.0f, -1000 +5* 1000 * cos(anglePlane2 *PI / 180))));
	Path planePath2 = Path(waypoints, 20.0f);
	Aircraft * plane2;
	glm::vec3 direction2 = glm::vec3(1000 * sin(anglePlane2 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane2 *PI / 180));
	if (scale2 == AircraftScale::big)
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if ((scale2 == AircraftScale::med))
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	else
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\C130\\C130.obj", AircraftScale::big);
	plane2->SetSpeed(50.0f);
	plane2->SetOrientation(0.0f, (GLfloat)anglePlane2, 0.0f);
	myPlanes.push_back(plane2);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	DrawPathsOnMatrix(widthOfAirspace);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 3 - GENERATE THREE PLANES ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generate3Planes(int width, AircraftScale scale1, AircraftScale scale2, AircraftScale scale3, int anglePlane1, int anglePlane2, int anglePlane3) {
	int widthOfAirspace = width;

	waypoints.push_back(new Waypoint(glm::vec3(-5 * 1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 + 5 * 1000 * cos(anglePlane1 *PI / 180))));
	Path planePath = Path(waypoints, 20.0f);
	Aircraft * plane;
	if (scale1 == AircraftScale::big)
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if ((scale1 == AircraftScale::med))
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	else
		plane = new Aircraft(glm::vec3(1000 * sin(anglePlane1 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane1 *PI / 180)), planePath, ".\\Models\\HellFire\\untitled.obj", AircraftScale::small);
	plane->SetSpeed(50.0f);
	plane->SetOrientation(0.0f, (GLfloat)anglePlane1, 0.0f);
	myPlanes.push_back(plane);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(-5 * 1000 * sin(anglePlane2 *PI / 180), 0.0f, -1000 + 5 * 1000 * cos(anglePlane2 *PI / 180))));
	Path planePath2 = Path(waypoints, 20.0f);
	Aircraft * plane2;
	glm::vec3 direction2 = glm::vec3(1000 * sin(anglePlane2 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane2 *PI / 180));
	if (scale2 == AircraftScale::big)
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if ((scale2 == AircraftScale::med))
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	else
		plane2 = new Aircraft(direction2, planePath2, ".\\Models\\Spitfire\\spitfire-for-export.obj", AircraftScale::small);
	plane2->SetSpeed(50.0f);
	plane2->SetOrientation(0.0f, (GLfloat)anglePlane2, 0.0f);
	myPlanes.push_back(plane2);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(-5 * 1000 * sin(anglePlane3 *PI / 180), 0.0f, -1000 + 5 * 1000 * cos(anglePlane3 *PI / 180))));
	Path planePath3 = Path(waypoints, 20.0f);
	Aircraft * plane3;
	glm::vec3 direction3 = glm::vec3(1000 * sin(anglePlane3 *PI / 180), 0.0f, -1000 - 1000 * cos(anglePlane3 *PI / 180));
	if (scale3 == AircraftScale::big)
		plane3 = new Aircraft(direction3, planePath3, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	else if ((scale3 == AircraftScale::med))
		plane3 = new Aircraft(direction3, planePath3, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	else
		plane3 = new Aircraft(direction3, planePath3, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::small);
	plane3->SetSpeed(50.0f);
	plane3->SetOrientation(0.0f, (GLfloat)anglePlane3, 0.0f);
	myPlanes.push_back(plane3);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	DrawPathsOnMatrix(widthOfAirspace);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 4 - AIRPORT /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generateAirportPlanes(int width) {

	int widthOfAirspace = width;
	float defaultWidth = 4000.0f;
	float scale = widthOfAirspace / defaultWidth;
	//plane 1
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	Path planePath1 = Path(waypoints, 20.0f);
	Aircraft* plane1 = new Aircraft(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f), planePath1, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane1->SetSpeed(50.0f);
	plane1->SetOrientation(0, 0, 0);
	myPlanes.push_back(plane1);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	//plane 2
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, -1500.0f)));
	Path planePath2 = Path(waypoints, 20.0f);
	Aircraft * plane2 = new Aircraft(scale*glm::vec3(1500.0f, 0.0f, -1500.0f), planePath2, ".\\Models\\plane\\plane2.obj", AircraftScale::big);
	plane2->SetSpeed(50.0f);
	plane2->SetOrientation(0, 90, 0);
	myPlanes.push_back(plane2);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	//plane 3
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, 1500.0f)));
	Path planePath3 = Path(waypoints, 20.0f);
	Aircraft * plane3 = new Aircraft(scale*glm::vec3(1500.0f, 0.0f, 1500.0f), planePath3, ".\\Models\\plane\\plane3.obj", AircraftScale::big);
	plane3->SetSpeed(50.0f);
	plane3->SetOrientation(0, 180, 0);
	myPlanes.push_back(plane3);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();


	//plane 4
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	Path planePath4 = Path(waypoints, 20.0f);
	Aircraft * plane4 = new Aircraft(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f), planePath4, ".\\Models\\C130\\C130.obj", AircraftScale::big);
	plane4->SetSpeed(50.0f);
	plane4->SetOrientation(0, 270, 0);
	myPlanes.push_back(plane4);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();


	//plane 5
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(500.0f, 0.0f, -500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(700.0f, 0.0f, 700.0f)));
	Path planePath5 = Path(waypoints, 20.0f);
	Aircraft * plane5 = new Aircraft(scale*glm::vec3(700.0f, 0.0f, 700.0f), planePath5, ".\\Models\\plane\\plane4.obj", AircraftScale::big);
	plane5->SetSpeed(50.0f);
	plane5->SetOrientation(0, 180, 0);
	myPlanes.push_back(plane5);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	//plane 5
	waypoints.clear();
	waypoints.push_back(new Waypoint(scale*glm::vec3(1100.0f, 0.0f, -1100.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, -1000.0f))); // touch down
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, -500.0f, 1000.0f))); // end runway
	waypoints.push_back(new Waypoint(scale*glm::vec3(-1500.0f, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(scale*glm::vec3(1100.0f, 0.0f, 1100.0f)));
	Path planePath6 = Path(waypoints, 20.0f);
	Aircraft * plane6 = new Aircraft(scale*glm::vec3(1100.0f, 0.0f, 1100.0f), planePath6, ".\\Models\\C130\\C130.obj", AircraftScale::big);
	plane6->SetSpeed(50.0f);
	plane6->SetOrientation(0, 180, 0);
	myPlanes.push_back(plane6);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	DrawPathsOnMatrix(widthOfAirspace);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 5 - RANDOM PLANES /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generateRandomPlanes(int width, int numPlanes) {

	int widthOfAirspace = width;
	int waypointSize = 4;
	srand(time(NULL));
	waypoints.clear();

	//As many planes as you want
	for (int i = 0; i < numPlanes; i++) {
		for (int j = 0; j < waypointSize; j++) {
			float x = (float)(rand() % widthOfAirspace - widthOfAirspace / 2);
			float z = (float)(rand() % widthOfAirspace - widthOfAirspace / 2);
			waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, z)));
		}

		Path planePath = Path(waypoints, 20.0f);
		//int planeType = 6;
		int planeType = rand() % 7;
		Aircraft* plane;
		if (planeType == 0) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::small);
			plane->SetSpeed(45.0f);
		}
		else if (planeType == 1) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
			plane->SetSpeed(65.0f);
		}
		else if (planeType == 2) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\Spitfire\\spitfire-for-export.obj", AircraftScale::med);
			plane->SetSpeed(40.0f);
		}
		else if (planeType == 3) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\C130\\C130.obj", AircraftScale::big);
			plane->SetSpeed(50.0f);
		}
		else if (planeType == 4) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\plane\\plane2.obj", AircraftScale::med);
			plane->SetSpeed(70.0f);
		}
		else if (planeType == 5) {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\vaderstuff\\vadertie.obj", AircraftScale::med);
			plane->SetSpeed(60.0f);
		}
		else {
			plane = new Aircraft(glm::vec3((float)(rand() % widthOfAirspace - widthOfAirspace / 2),
				0.0f, (float)(rand() % widthOfAirspace - widthOfAirspace / 2)), planePath, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::med);
			plane->SetSpeed(2.0f);
		}
		myPlanes.push_back(plane);
		//std::cout << "(Pgen.cpp line 456) waypoints.size = " << waypoints.size() << std::endl;;
		for (int i = 0; i < waypoints.size(); i++) {

			points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
		}
		planePoints.push_back(points);
		points.clear();
		std::cout << "size of myPlanes = " << myPlanes.size() << std::endl;
		waypoints.clear();

	}
	DrawPathsOnMatrix(widthOfAirspace);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// OPTION 6 - AIRSPACE PLANES ///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::generateAirspacePlanes(int width) {

	int widthOfAirspace = width;
	////plane 1
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-400.0f, 0.0f, 900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-300.0f, 0.0f, 1400.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(400.0f, 0.0f, -900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 0.0f, -1400.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -500.0f)));
	Path planePath = Path(waypoints, 20.0f);
	Aircraft* plane = new Aircraft(glm::vec3(0.0f, 0.0f, -1500.0f), planePath, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane->SetSpeed(50.0f);
	myPlanes.push_back(plane);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	//plane 2
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(1000.0f, 0.0f, 0.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(250.0f, 0.0f, 1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-250.0f, 0.0f, 1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-1000.0f, 0.0f, -0.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-250.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(250.0f, 0.0f, -1000.0f)));
	Path planePath2 = Path(waypoints, 20.0f);
	Aircraft* plane2 = new Aircraft(glm::vec3(0.0f, 0.0f, -100.0f), planePath2, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane2->SetSpeed(100.0f);
	myPlanes.push_back(plane2);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();


	//plane 3
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(1000.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-1000.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(1000.0f, 0.0f, 1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-1000.0f, 0.0f, 1000.0f)));
	Path planePath3 = Path(waypoints, 20.0f);
	Aircraft* plane3 = new Aircraft(glm::vec3(0.0f, 0.0f, -200.0f), planePath3, ".\\Models\\vought\\vought.obj", AircraftScale::small);
	plane3->SetSpeed(50.0f);
	plane3->SetOrientation(0.0f, 180.0f, 0.0f);
	myPlanes.push_back(plane3);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();


	//plane 4
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -600.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 0.0f, 600.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-300.0f, 0.0f, 600.0f)));
	Path planePath4 = Path(waypoints, 20.0f);
	Aircraft* plane4 = new Aircraft(glm::vec3(0.0f, 0.0f, -400.0f), planePath4, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	plane4->SetSpeed(80.0f);
	myPlanes.push_back(plane4);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();


	//plane 5
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-400.0f, 0.0f, 900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-300.0f, 0.0f, 400.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(400.0f, 0.0f, -900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(400.0f, 0.0f, -900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 0.0f, -400.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -1000.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, 500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-400.0f, 0.0f, 900.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(20.0f, 0.0f, -100.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 0.0f, -500.0f)));
	Path planePath5 = Path(waypoints, 20.0f);
	Aircraft* plane5 = new Aircraft(glm::vec3(0.0f, 0.0f, -1000.0f), planePath5, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane5->SetSpeed(100.0f);
	myPlanes.push_back(plane5);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	//plane 6
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(-1500, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(1500, 0.0f, 1500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(1500, 0.0f, -1500.0f)));
	waypoints.push_back(new Waypoint(glm::vec3(-1500, 0.0f, 1500.0f)));
	Path planePath6 = Path(waypoints, 20.0f);
	Aircraft* plane6 = new Aircraft(glm::vec3(-1500.0f, 0.0f, -1500.0f), planePath6, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane6->SetSpeed(100.0f);
	myPlanes.push_back(plane6);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();

	DrawPathsOnMatrix(widthOfAirspace);

}

void PlaneGenerator::generateApproachingPlanes(int width) {
	cout << "Enter the number of approaching planes...";
	int numberOfPlanes;
	cin >> numberOfPlanes;

	int widthOfAirspace = width;
	int start = -2500;

	//As many planes as you want
	for (int i = 0; i < numberOfPlanes; i++) {
		waypoints.clear();
		float x = (float)(rand() % widthOfAirspace - widthOfAirspace / 2);
		float y = (float)(rand() % 200 - 100);
		waypoints.push_back(new Waypoint(glm::vec3(x, y, start)));
		points.push_back(Point(x, start));
		waypoints.push_back(new Waypoint(glm::vec3(x, y, start + 13000)));
		points.push_back(Point(x, start + 13000));

		Path planePath = Path(waypoints, 20.0f);
		int planeType = rand() % 3;
		Aircraft* plane;
		if (planeType == 0)
			plane = new Aircraft(glm::vec3(x, y, start), planePath, ".\\Models\\plane\\plane3.obj", AircraftScale::small);
		else if (planeType == 1)
			plane = new Aircraft(glm::vec3(x, y, start), planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
		else
			plane = new Aircraft(glm::vec3(x, y, start), planePath, ".\\Models\\C130\\C130.obj", AircraftScale::big);
		plane->SetSpeed(50.0f);
		myPlanes.push_back(plane);
		planePoints.push_back(points);
		points.clear();
		start -= 100;
	}
	DrawPathsOnMatrix(widthOfAirspace);

}

//*********************************************************2017 additions*******************************************************
//////////////////////////////////// display case, repetitve circular testing functions below //////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// OPTION 7 - DISPLAY PLANES /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::displayPlanes(int width) {

	int widthOfAirspace = width;
	
	////plane 1
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 00.0f, 500.0f)));
	
	Path planePath = Path(waypoints, 20.0f);
	Aircraft* plane = new Aircraft(glm::vec3(00.0f, 00.0f, 0.0f), planePath, ".\\Models\\plane\\plane.obj", AircraftScale::big);
	plane->SetSpeed(00.0f);
	myPlanes.push_back(plane);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 1 CREATED!" << std::endl;

	//plane 2
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 30.0f, 500.0f)));
	
	Path planePath2 = Path(waypoints, 20.0f);
	Aircraft* plane2 = new Aircraft(glm::vec3(0.0f, 30.0f, 00.0f), planePath2, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::med);
	plane2->SetSpeed(0.0f);
	myPlanes.push_back(plane2);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 2 CREATED!" << std::endl;


	//plane 3
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 60.0f, 500.0f)));
	
	Path planePath3 = Path(waypoints, 20.0f);
	Aircraft* plane3 = new Aircraft(glm::vec3(0.0f, 60.0f, 00.0f), planePath3, ".\\Models\\C130\\C130.obj", AircraftScale::big);
	plane3->SetSpeed(0.0f);
	//plane3->SetOrientation(0.0f, 180.0f, 0.0f);
	myPlanes.push_back(plane3);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 3 CREATED!" << std::endl;


	//plane 4
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 90.0f, 500.0f)));
	
	Path planePath4 = Path(waypoints, 20.0f);
	Aircraft* plane4 = new Aircraft(glm::vec3(0.0f, 90.0f, 00.0f), planePath4, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
	plane4->SetSpeed(0.0f);
	myPlanes.push_back(plane4);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 4 CREATED!" << std::endl;


	//plane 5
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 150.0f, 500.0f)));

	Path planePath5 = Path(waypoints, 20.0f);
	Aircraft* plane5 = new Aircraft(glm::vec3(0.0f, 150.0f, 00.0f), planePath5, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::big);
	plane5->SetSpeed(0.0f);
	myPlanes.push_back(plane5);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 5 CREATED!" << std::endl;

	//plane 6
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, 120.0f, 500.0f)));
	
	Path planePath6 = Path(waypoints, 20.0f);
	Aircraft* plane6 = new Aircraft(glm::vec3(0.0f, 120.0f, 00.0f), planePath6, ".\\Models\\Hellfire\\untitled.obj", AircraftScale::med);
	plane6->SetSpeed(0.0f);
	myPlanes.push_back(plane6);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 6 CREATED!" << std::endl;

	DrawPathsOnMatrix(widthOfAirspace);

	//plane 7
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(0.0f, -60.0f, 500.0f)));

	Path planePath7 = Path(waypoints, 20.0f);
	Aircraft* plane7 = new Aircraft(glm::vec3(0.0f, -60.0f, 00.0f), planePath7, ".\\Models\\plane\\plane3.obj", AircraftScale::big);
	plane7->SetSpeed(0.0f);
	myPlanes.push_back(plane7);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 7 CREATED!" << std::endl;

	////plane 8
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 00.0f, 500.0f)));

	Path planePath8 = Path(waypoints, 20.0f);
	Aircraft* plane8 = new Aircraft(glm::vec3(300.0f, 00.0f, 0.0f), planePath8, ".\\Models\\plane\\plane2.obj", AircraftScale::big);
	plane8->SetSpeed(00.0f);
	myPlanes.push_back(plane8);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 8 CREATED!" << std::endl;

	////plane 9
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 40.0f, 500.0f)));

	Path planePath9 = Path(waypoints, 20.0f);
	Aircraft* plane9 = new Aircraft(glm::vec3(300.0f, 40.0f, 0.0f), planePath9, ".\\Models\\plane\\plane4.obj", AircraftScale::big);
	plane9->SetSpeed(00.0f);
	myPlanes.push_back(plane9);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 9 CREATED!" << std::endl;

	////plane 10
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, -40.0f, 500.0f)));

	Path planePath10 = Path(waypoints, 20.0f);
	Aircraft* plane10 = new Aircraft(glm::vec3(300.0f, -40.0f, 0.0f), planePath10, ".\\Models\\vaderstuff\\vadertie.obj", AircraftScale::med);
	plane10->SetSpeed(00.0f);
	myPlanes.push_back(plane10);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 10 CREATED!" << std::endl;

	////plane 11
	waypoints.clear();
	waypoints.push_back(new Waypoint(glm::vec3(300.0f, 80.0f, 500.0f)));

	Path planePath11 = Path(waypoints, 20.0f);
	Aircraft* plane11 = new Aircraft(glm::vec3(300.0f, 80.0f, 0.0f), planePath11, ".\\Models\\Spitfire\\spitfire-for-export.obj", AircraftScale::med);
	plane11->SetSpeed(00.0f);
	myPlanes.push_back(plane11);
	for (int i = 0; i < waypoints.size(); i++)
		points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
	planePoints.push_back(points);
	points.clear();
	std::cout << "PLANE 11 CREATED!" << std::endl;

	//blocks at 50 units apart to measure scale of aircraft
	/*
	for (int i = -120; i < 200; i += 10) {
		float j = (float)i;
		waypoints.clear();
		waypoints.push_back(new Waypoint(glm::vec3(100.0f, j, 500.0f)));
		Path blockP = Path(waypoints, 20.0f);
		Aircraft* blockA = new Aircraft(glm::vec3(100.0f, j, 00.0f), blockP, ".\\Models\\cube\\cube.obj", AircraftScale::big);
		blockA->SetSpeed(0.0f);
		myPlanes.push_back(blockA);
	}
	for (int i = -120; i < 200; i += 10) {
		float j = (float)i;
		waypoints.clear();
		waypoints.push_back(new Waypoint(glm::vec3(-100.0f, j, 500.0f)));
		Path blockP = Path(waypoints, 20.0f);
		Aircraft* blockA = new Aircraft(glm::vec3(-100.0f, j, 00.0f), blockP, ".\\Models\\cube\\cube.obj", AircraftScale::big);
		blockA->SetSpeed(0.0f);
		myPlanes.push_back(blockA);	
	}
	for (int i = -120; i < 200; i += 10) {
		float j = (float)i;
		waypoints.clear();
		waypoints.push_back(new Waypoint(glm::vec3(-50.0f, j, 500.0f)));
		Path blockP = Path(waypoints, 20.0f);
		Aircraft* blockA = new Aircraft(glm::vec3(-50.0f, j, 00.0f), blockP, ".\\Models\\cube\\cube.obj", AircraftScale::big);
		blockA->SetSpeed(0.0f);
		myPlanes.push_back(blockA);	
	}
	for (int i = -120; i < 200; i += 10) {
		float j = (float)i;
		waypoints.clear();
		waypoints.push_back(new Waypoint(glm::vec3(50.0f, j, 500.0f)));
		Path blockP = Path(waypoints, 20.0f);
		Aircraft* blockA = new Aircraft(glm::vec3(50.0f, j, 00.0f), blockP, ".\\Models\\cube\\cube.obj", AircraftScale::big);
		blockA->SetSpeed(0.0f);
		myPlanes.push_back(blockA);
	}
	*/
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// OPTION 8 - PROJECT RUNWAY /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void PlaneGenerator::projectRunway(int width, int numPlanes) {
	int widthOfAirspace = width;
	int waypointSize = 1;
	srand(time(NULL));
	waypoints.clear();
	float x = -2500.0, z = 2500.0;

	//As many planes as you want
	for (int i = 0; i < numPlanes; i++) {
		for (int j = 0; i < 25; i++) {
			waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, z)));
			waypoints.push_back(new Waypoint(glm::vec3(-x, 0.0f, -z)));

			x = x + 200.0;
		}
		x = 2500.0; z = 2500.0;
		for (int j = 0; j < 25; j++) {
			waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, z)));
			waypoints.push_back(new Waypoint(glm::vec3(-x, 0.0f, -z)));

			z = z - 200.0;
		}

		Path planePath = Path(waypoints, 20.0f);
		//int planeType = 6;
		int planeType = rand() % 7;
		Aircraft* plane;
		if (planeType == 0) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::small);
			plane->SetSpeed(45.0f);
		}
		else if (planeType == 1) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
			plane->SetSpeed(65.0f);
		}
		else if (planeType == 2) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\Spitfire\\spitfire-for-export.obj", AircraftScale::med);
			plane->SetSpeed(40.0f);
		}
		else if (planeType == 3) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\C130\\C130.obj", AircraftScale::big);
			plane->SetSpeed(50.0f);
		}
		else if (planeType == 4) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\plane\\plane2.obj", AircraftScale::med);
			plane->SetSpeed(70.0f);
		}
		else if (planeType == 5) {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\vaderstuff\\vadertie.obj", AircraftScale::med);
			plane->SetSpeed(60.0f);
		}
		else {
			plane = new Aircraft(glm::vec3(00.0f, 0.0f, -1000.0f), planePath, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::med);
			plane->SetSpeed(2.0f);
		}
		myPlanes.push_back(plane);
		//std::cout << "(Pgen.cpp line 456) waypoints.size = " << waypoints.size() << std::endl;;
		for (int i = 0; i < waypoints.size(); i++) {

			points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
		}
		planePoints.push_back(points);
		points.clear();
		//std::cout << "size of myPlanes = " << myPlanes.size() << std::endl;
		waypoints.clear();

	}
	DrawPathsOnMatrix(widthOfAirspace);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// OPTION 9 & 11 - CIRCULAR GAUNTLET 1 & 2 ///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::circleGauntlet(int width, int numPlanes) {

	int widthOfAirspace = width;
	int waypointSize = 1;
	srand(time(NULL));
	waypoints.clear();
	float x, z;

	if ((Camera::getSimChoice() == 11) && (myPlanes.size() >= PARTS)) {
		return;
	}


	//As many planes as you want
	for (int i = 0; i < numPlanes; i++) {
		for (int j = 0; j < 360; j=j+ (int)(360/PARTS)) {
			x = 2000 * cos((float)((j + 90) % 360)* PI/180.0);
			z = 2000 * sin((float)((j + 90) % 360)* PI/180.0);
			waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, (z - 1000.0f))));
			//waypoints.push_back(new Waypoint(glm::vec3(-x, 0.0f, -z)));
		}

		Path planePath = Path(waypoints, 20.0f);
		//int planeType = 6;
		glm::vec3 startingPoint = glm::vec3(00.0f, 0.0f, -3000.0f);
		int planeType = rand() % 7;
		if (planeType == 6 && i == 0) {
			planeType = rand() % 6;
		}
		Aircraft* plane;
		if (planeType == 0) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::med);
			plane->SetSpeed(45.0f);
		}
		else if (planeType == 1) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
			plane->SetSpeed(60.0f);
		}
		else if (planeType == 2) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\HellFire\\untitled.obj", AircraftScale::med);
			plane->SetSpeed(40.0f);
		}
		else if (planeType == 3) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\C130\\C130.obj", AircraftScale::big);
			plane->SetSpeed(50.0f);
		}
		else if (planeType == 4) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\plane\\plane2.obj", AircraftScale::big);
			plane->SetSpeed(60.0f);
		}
		else if (planeType == 5) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\plane\\plane.obj", AircraftScale::med);
			plane->SetSpeed(60.0f);
		}
		else {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::med);
			plane->SetSpeed(30.0f);
		}
		myPlanes.push_back(plane);
		//std::cout << "(Pgen.cpp line 456) waypoints.size = " << waypoints.size() << std::endl;;
		for (int i = 0; i < (int)waypoints.size(); i++) {

			points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
		}
		planePoints.push_back(points);
		points.clear();
		//std::cout << "size of myPlanes = " << myPlanes.size() << std::endl;
		waypoints.clear();

	}
	DrawPathsOnMatrix(widthOfAirspace);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// OPTION 10 - SLOW ROLL /// /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::slowRoll(int width, int numPlanes) {

	int widthOfAirspace = width;
	srand(time(NULL));
	waypoints.clear();
	float x, z;	

	//As many planes as you want
	for (int i = 0; i < numPlanes; i++){
		//std::cout << "Number of planes = " << myPlanes.size() << std::endl;
		if (myPlanes.size() % 2 == 0) {
			//std::cout << "clockwise circle" << std::endl;
			for (int j = 0; j < 360; j = j + (int)(360/PARTS)) {
				x = 2000 * cos((float)((j + 90) % 360)* PI / 180.0);
				z = 2000 * sin((float)((j + 90) % 360)* PI / 180.0);
				waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, (z - 1000.0f))));
			}
		}else if (myPlanes.size() % 2 != 0){
			//std::cout << "counterclockwise circle" << std::endl;
			for (int j = 0; j > -360; j = j - (int)(360 /PARTS)) {
				x = 2000 * cos((float)((j + 90) % 360)* PI / 180.0);
				z = 2000 * sin((float)((j + 90) % 360)* PI / 180.0);
				waypoints.push_back(new Waypoint(glm::vec3(x, 0.0f, (z - 1000.0f))));
			}
		}

		Path planePath = Path(waypoints, 20.0f);
		//int planeType = 6;
		glm::vec3 startingPoint = glm::vec3(00.0f, 0.0f, -3000.0f);
		int planeType = rand() % 7;
		Aircraft* plane;
		if (planeType == 0) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\MQ9Reaper\\mq9Reaper.obj", AircraftScale::med);
			plane->SetSpeed(90.0f);
		}
		else if (planeType == 1) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\fighter\\fighter.obj", AircraftScale::med);
			plane->SetSpeed(120.0f);
		}
		else if (planeType == 2) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\HellFire\\untitled.obj", AircraftScale::med);
			plane->SetSpeed(80.0f);
		}
		else if (planeType == 3) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\C130\\C130.obj", AircraftScale::big);
			plane->SetSpeed(100.0f);
		}
		else if (planeType == 4) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\plane\\plane2.obj", AircraftScale::big);
			plane->SetSpeed(120.0f);
		}
		else if (planeType == 5) {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\plane\\plane.obj", AircraftScale::med);
			plane->SetSpeed(120.0f);
		}
		else {
			plane = new Aircraft(startingPoint, planePath, ".\\Models\\HotAirBalloon\\Hotairballon.obj", AircraftScale::med);
			plane->SetSpeed(60.0f);
		}
		myPlanes.push_back(plane);
		//std::cout << "(Pgen.cpp line 456) waypoints.size = " << waypoints.size() << std::endl;;
		for (int i = 0; i < (int)waypoints.size(); i++) {

			points.push_back(Point(waypoints.at(i)->GetPosition().x, waypoints.at(i)->GetPosition().z));
		}
		planePoints.push_back(points);
		points.clear();
		//std::cout << "size of myPlanes = " << myPlanes.size() << std::endl;
		waypoints.clear();

	}
	std::cout << "Number of planes = " << myPlanes.size() << std::endl;
	DrawPathsOnMatrix(widthOfAirspace);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// OPEN CV MAP WINDOW ///////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function takes our planes and draws a top down view of each of their paths. It also creates a total airspace flight path for all airplanes. 
//There are several loops in this function because we are iterating through all of the vectors we have available for our airplanes.
void PlaneGenerator::DrawPathsOnMatrix(int widthOfAirspace) {
	planePaths.clear();
	int numberOfPlanes = myPlanes.size();
	//std::cout << "!!!!!! line 511 - numberOfPlanes = !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << numberOfPlanes << std::endl;
	//Mat completeDrawing(500, 500, CV_8UC3, Scalar(255, 255, 255)); //Matrix for complete matrix with all paths
	Mat completeDrawing = imread("background.png");
	for (int i = 0; i < numberOfPlanes; i++) {
		Mat individual(500, 500, CV_8UC3, Scalar(255, 255, 255)); //Matrix for indvidual plane matrix with single paths
		stringstream width; width << widthOfAirspace;
		putText(individual, "Size of Airspace: " + width.str(), Point(0, 20), FONT_HERSHEY_DUPLEX, 0.5, Scalar(0, 0, 0)); //Print size of airspace on the Matrix
		int red = rand() % 255; int green = rand() % 255; int blue = rand() % 255; //random colors
		for (int j = 1; j <= planePoints.at(i).size(); j++) { //iteratre through all points to that individual plane
															  //std::cout << j << " (j)plane generator.cpp 520: planepoints.at(i) = " << planePoints.at(i).size() << std::endl;
			if (j != planePoints.at(i).size()) { //draw lines accordingly on both matrices, while also puting point coordinates on the individual matrix
				MyLine(completeDrawing, planePoints.at(i).at(j), planePoints.at(i).at(j - 1), red, green, blue, widthOfAirspace);
				MyLine(individual, planePoints.at(i).at(j), planePoints.at(i).at(j - 1), red, green, blue, widthOfAirspace);
				stringstream x, y;
				x << planePoints.at(i).at(j - 1).x;
				y << planePoints.at(i).at(j - 1).y;
				putText(individual, "(" + x.str() + " , " + y.str() + ")", Point((planePoints.at(i).at(j - 1).x + widthOfAirspace / 2) / (widthOfAirspace / 500), (planePoints.at(i).at(j - 1).y + widthOfAirspace / 2) / (widthOfAirspace / 500)), FONT_HERSHEY_PLAIN, 1, Scalar(red, green, blue));
			}
			else {
				MyLine(completeDrawing, planePoints.at(i).at(j - 1), planePoints.at(i).at(0), red, green, blue, widthOfAirspace);
				MyLine(individual, planePoints.at(i).at(j - 1), planePoints.at(i).at(0), red, green, blue, widthOfAirspace);
				stringstream x, y;
				x << planePoints.at(i).at(j - 1).x;
				y << planePoints.at(i).at(j - 1).y;
				putText(individual, "(" + x.str() + " , " + y.str() + ")", Point((planePoints.at(i).at(j - 1).x + widthOfAirspace / 2) / (widthOfAirspace / 500), (planePoints.at(i).at(j - 1).y + widthOfAirspace / 2) / (widthOfAirspace / 500)), FONT_HERSHEY_PLAIN, 1, Scalar(red, green, blue));
			}
		}
		planePaths.push_back(individual); //push individual matrix to the vector
	}
	planePaths.push_back(completeDrawing); //push complete matrix to the drawing

}

/////////////////////////////////// DRAWS PATH LINE BETWEEN WAYPOINTS ///////////////////////////////////////////
void PlaneGenerator::MyLine(Mat img, Point start, Point end, int red, int green, int blue, int widthOfAirspace)
{
	int thickness = 2.0;
	int lineType = 8;
	line(img,
		Point((start.x + widthOfAirspace / 2) / (widthOfAirspace / 500), (start.y + widthOfAirspace / 2) / (widthOfAirspace / 500)),
		Point((end.x + widthOfAirspace / 2) / (widthOfAirspace / 500), (end.y + widthOfAirspace / 2) / (widthOfAirspace / 500)),
		Scalar(red, green, blue),
		thickness,
		lineType);
}

vector< Aircraft*> PlaneGenerator::getPlanes() {
	return myPlanes;
}

vector< Mat> PlaneGenerator::getPlanePaths() {
	return planePaths;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////// MENU OPTION FUNCTION TO CHOOSE SIMULATION //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaneGenerator::chooseSim(int widthOfAirspace, AircraftScale scale1, AircraftScale scale2, int& choice) {

	std::cout << "[Monday version] What would you like to test against: " << std::endl << std::endl
		<< "[1] one plane" << std::endl
		<< "[2] two planes" << std::endl
		<< "[3] three planes" << std::endl
		<< "[4] airport" << std::endl
		<< "[5] random" << std::endl
		<< "[6] airspace " << std::endl
		<< std::endl
		<< "[7] 2017 See a planes display" << std::endl
		<< "[8] 2017 edition - round we go" << std::endl
		<< "[9] 2017 circular gauntlet" << std::endl
		<< "[10] 2017 Slow Roll" << std::endl
		<< "[11] 2017 circular gauntlet 2" << std::endl;
	do {
		std::cin >> choice;
	} while ((choice < 1 || choice > 11) || (choice != int(choice)));

	int angle1, angle2, angle3;
	switch (choice) {
	case 1:
		std::cout << "Enter an angle: ";
		std::cin >> angle1;
		generate1Plane(widthOfAirspace, angle1);
		break;
	case 2:
		std::cout << "Enter angle 1: ";
		std::cin >> angle1;
		std::cout << "Enter angle 2: ";
		std::cin >> angle2;
		generate2Planes(widthOfAirspace, scale1, scale2, angle1, angle2);
		break;
	case 3:
		std::cout << "Enter angle 1: ";
		std::cin >> angle1;
		std::cout << "Enter angle 2: ";
		std::cin >> angle2;
		std::cout << "Enter angle 3: ";
		std::cin >> angle3;
		generate3Planes(widthOfAirspace, scale1, scale2, AircraftScale::big, angle1, angle2, angle3);
		break;
	case 4:
		generateAirportPlanes(widthOfAirspace);
		break;
	case 5:
		int numPlanes;
		std::cout << std::endl;
		std::cout << "Enter number of planes: ";
		std::cin >> numPlanes;
		generateRandomPlanes(widthOfAirspace, numPlanes);
		break;
	case 6:
		generateAirspacePlanes(widthOfAirspace);
		break;
	case 7:
		displayPlanes(widthOfAirspace);
		break;
	case 8:
		projectRunway(widthOfAirspace, 1);
		break;
	case 9:
		std::cout << "How many parts: ";
		std::cin >> PARTS;
		circleGauntlet(widthOfAirspace, 1);
		break;
	case 10: 
		std::cout << "How many parts: ";
		std::cin >> PARTS;
		slowRoll(widthOfAirspace, 1);
		break;
	case 11:
		std::cout << "How many parts: ";
		std::cin >> PARTS;
		circleGauntlet(widthOfAirspace, 1);
		break;
	}
}

int PlaneGenerator::getParts() {
	return PARTS;
}

//CURRENTLY EXCLUDED FUNCTION ******************
//APPROACHING PLANES ***************************

